public class TestAccount2 {
    public static void main(String[] args) {
        Account[] myAccounts = new Account[5];
        String[] names = {"a","b", "c","d", "e"};
        double[] balances = {1,2,3,4,5};
        for (int i=0; i < myAccounts.length; i++) {
            myAccounts[i] = new SavingsAccount(names[i], balances[i]);
            //System.out.println("default account "+myAccounts[i].toString());
            System.out.println("default name "+myAccounts[i].getName());
            System.out.println("default balance "+myAccounts[i].getBalance());
            myAccounts[i].addInterest();
            System.out.println("current balance "+myAccounts[i].getBalance());
        }

        System.out.println("default ir "+Account.getInterestRate());
        Account.setInterestRate(0.2);
        System.out.println("current ir "+Account.getInterestRate());

        System.out.println("withdraw money without a parameter " + myAccounts[0].withdraw());
        System.out.println("withdraw money with a parameter " + myAccounts[0].withdraw(5));

        if (! myAccounts[0].withdraw(5)){
            System.out.println("Your transaction failed");
        }
        
    }
    
}