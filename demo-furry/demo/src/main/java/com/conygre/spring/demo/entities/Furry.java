package com.conygre.spring.demo.entities;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Furry {

    private ObjectId id;
    private String animal;
    private String name;

    public Furry(ObjectId id, String animal, String name) {
        this.id = id;
        this.animal = animal;
        this.name = name;
    }

    public Furry() {
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    

    


}