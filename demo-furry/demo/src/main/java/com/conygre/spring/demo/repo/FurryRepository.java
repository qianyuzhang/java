package com.conygre.spring.demo.repo;

import com.conygre.spring.demo.entities.Furry;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FurryRepository extends MongoRepository<Furry, ObjectId> {
    
}