package com.conygre.spring.demo.controllers;

import java.util.Collection;

import com.conygre.spring.demo.entities.Furry;
import com.conygre.spring.demo.service.FurryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/furries")
@CrossOrigin
public class FurryController {

    @Autowired
    private FurryService service;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Furry> getFurries(){
        return service.getFurries();
    }

    @RequestMapping(method = RequestMethod.POST)
    //@PostMethod
    public void addFurry(@RequestBody Furry furry) {
        service.addFurry(furry);

    }
}