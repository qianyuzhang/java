package com.conygre.spring.demo.service;

import java.util.Collection;

import com.conygre.spring.demo.entities.Furry;
import com.conygre.spring.demo.repo.FurryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FurryServiceImpl implements FurryService {

    @Autowired
    private FurryRepository repo;

    @Override
    public Collection<Furry> getFurries() {
        
        return repo.findAll();
    }

    @Override
    public void addFurry(Furry furry) {
        repo.insert(furry);

    }
    

}