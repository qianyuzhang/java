package com.conygre.spring.demo.service;

import java.util.Collection;

import com.conygre.spring.demo.entities.Furry;


public interface FurryService {
    
    Collection<Furry> getFurries();
    void addFurry(Furry furry);
    
}