public class Car {
    

    public Car(){} //the default is taken out, declare again

    //properties
    private String make;
    private String model;
    //private int speed;

    //setter-getters JavaBeans convention
    public void setMake(String make) {
        this.make = make;
    }

    public String getMake(){
        return make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel(){
        return model;
    }

    public Car(String make, String model) {
        this.make = make;
        this.model = model;
    }


}