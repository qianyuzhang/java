public class SavingsAccount extends Account{

    public SavingsAccount(String n, double b) {
        super(n, b);
    }
    
    @Override
    public void addInterest(){
        this.setBalance(getBalance() * 1.4); 
    }
    
}