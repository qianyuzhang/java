public class CurrentAccount extends Account{

    public CurrentAccount(String n, double b) {
        super(n, b);
    }
    
    @Override
    public void addInterest(){
        this.setBalance(getBalance() * 1.1); 
    }
}