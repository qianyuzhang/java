public class HomeInsurance implements Detailable{
    private double premium;
    private double excess;
    private double amountInsured;

    public HomeInsurance(double premium, double excess, double amountInsured) {
        this.premium = premium;
        this.excess = excess;
        this.amountInsured = amountInsured;
    }

    @Override
    public void getDetails() {
        System.out.println("The premium: " + premium + ". The excess: " + excess);
        
    }

    
}