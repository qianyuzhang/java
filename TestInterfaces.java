public class TestInterfaces {
    public static void main(String[] args) {
        Detailable[] details = {new CurrentAccount("amber", 50), new SavingsAccount("amber", 100), new HomeInsurance(20, 30, 200)};
        for (Detailable d: details){
            d.getDetails();
        }
    }
    
}