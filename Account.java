public abstract class Account implements Detailable{

    
    //properties
    private String name;
    private double balance;

    public Account(String n, double b) throws DodgyNameException {
        this.name = n;
        this.balance = b;
    }

    
    // public Account() {
    //     this("Amber", 50);

    // }

    @Override
    public void getDetails() {
        System.out.println("The account name: " + name + ". The balance: " + balance);
    }

    // instance methods
	public void setBalance(double d) {
		balance = d;
	}

	public double getBalance() {
		return balance;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String s) throws DodgyNameException
	{
		name = s;
	}

    private static double interestRate = 0.1;

    public static void setInterestRate(double ir) {
        interestRate = ir;
    }

    public static double getInterestRate(){
        return interestRate;
    }

    public abstract void addInterest(); 
    /*
    public abstract void addInterest(){
        balance += balance * interestRate; //chap 8

    }
    */

    public boolean withdraw(double amount){
        if (balance >= amount){
            balance -= amount;
            return true;
        } else {
            return false;
        }
    }

    public boolean withdraw(){
        return this.withdraw(20);
        
    }

    


    
    
}