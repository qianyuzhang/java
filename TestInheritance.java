public class TestInheritance {
    public static void main(String[] args) {
        Account[] newAccounts = {new SavingsAccount("ab", 2), new SavingsAccount("cd", 4), new CurrentAccount("ef", 6)};
        for(Account a:newAccounts){
            System.out.println("default name "+a.getName());
            System.out.println("default balance "+a.getBalance());
            a.addInterest();
            System.out.println("current balance "+a.getBalance());     
        }
    }
    
}